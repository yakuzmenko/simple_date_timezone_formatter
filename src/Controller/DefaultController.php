<?php

declare(strict_types=1);

namespace App\Controller;

use App\DTO\DateFormDTO;
use App\Form\DateFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController
{
    /**
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request): Response
    {
        $form = $this->createForm(DateFormType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var DateFormDTO $dto */
            $dto = $form->getData();

            return $this->render('default/result.html.twig', [
                'date' => $dto->date,
                'timezone' => $dto->timezone,
            ]);
        }

        return $this->render('default/form.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
