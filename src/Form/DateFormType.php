<?php

declare(strict_types=1);

namespace App\Form;

use App\DTO\DateFormDTO;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class DateFormType extends AbstractType
{
    private const DATE_FORMAT = 'Y-m-d';
    private const DATE_PLACEHOLDER = '2000-01-01';
    private const TIMEZONE_PLACEHOLDER = 'Europe/London';

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date', TextType::class, [
                'label' => 'Date in UTC timezone',
                'attr' => [
                    'placeholder' => self::DATE_PLACEHOLDER,
                ],
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('timezone', TextType::class, [
                'label' => 'Your current timezone',
                'attr' => [
                    'placeholder' => self::TIMEZONE_PLACEHOLDER,
                ],
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Submit',
                'attr' => [
                    'class' => 'btn btn-primary'
                ],
            ])
        ;

        $builder->get('date')
            ->addModelTransformer(new CallbackTransformer(
                function (?\DateTime $date) {
                    return $date ? $date->format(self::DATE_FORMAT) : null;
                },
                function (?string $date) {
                    return $date ? new \DateTime($date, new \DateTimeZone('UTC')) : null;
                }
            ));

        $builder->get('timezone')
            ->addModelTransformer(new CallbackTransformer(
                function (?\DateTimeZone $timezone) {
                    return $timezone ? $timezone->getName() : null;
                },
                function (?string $timezone) {
                    return $timezone ? new \DateTimeZone($timezone) : null;
                }
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DateFormDTO::class,
        ]);
    }
}
