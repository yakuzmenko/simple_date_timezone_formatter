<?php

declare(strict_types=1);

namespace App\Enum;

class MonthEnum implements EnumerationInterface
{
    public const JANUARY = 'january';
    public const FEBRUARY = 'february';
    public const MARCH = 'march';
    public const APRIL = 'april';
    public const MAY = 'may';
    public const JUNE = 'june';
    public const JULY = 'july';
    public const AUGUST = 'august';
    public const SEPTEMBER = 'september';
    public const OCTOBER = 'october';
    public const NOVEMBER = 'november';
    public const DECEMBER = 'december';

    /**
     * {@inheritdoc}
     */
    public static function getItems(): array
    {
        return [
            self::JANUARY => 1,
            self::FEBRUARY => 2,
            self::MARCH => 3,
            self::APRIL => 4,
            self::MAY => 5,
            self::JUNE => 6,
            self::JULY => 7,
            self::AUGUST => 8,
            self::SEPTEMBER => 9,
            self::OCTOBER => 10,
            self::NOVEMBER => 11,
            self::DECEMBER => 12,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function isValidItem(string $item): bool
    {
        return array_key_exists($item, self::getItems());
    }

    /**
     * @param string $item
     *
     * @return int
     */
    public static function getNumber(string $item): int
    {
        if (!self::isValidItem($item)) {
            throw new \InvalidArgumentException(sprintf(
                'Invalid value "%s". Use one of following values: %s',
                $item,
                implode(', ', self::getItems())
            ));
        }

        return self::getItems()[$item];
    }
}
