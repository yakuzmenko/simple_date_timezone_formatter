<?php

declare(strict_types=1);

namespace App\Enum;

/**
 * Generic interface for enumerations
 */
interface EnumerationInterface
{
    /**
     * @return iterable
     */
    public static function getItems(): iterable;

    /**
     * @param string $item
     *
     * @return bool
     */
    public static function isValidItem(string $item): bool;
}
