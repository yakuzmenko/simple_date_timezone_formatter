<?php

declare(strict_types=1);

namespace App\DTO;

class DateFormDTO
{
    public \DateTime $date;

    public \DateTimeZone $timezone;
}
