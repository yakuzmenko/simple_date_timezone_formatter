<?php

declare(strict_types=1);

namespace App\Twig;

use App\Enum\MonthEnum;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('minutesOffset', [$this, 'formatMinutesOffset']),
            new TwigFunction('daysInMonth', [$this, 'getDaysInMonth']),
        ];
    }

    /**
     * @param \DateTime     $date
     * @param \DateTimeZone $timezone
     *
     * @return string
     */
    public function formatMinutesOffset(\DateTime $date, \DateTimeZone $timezone): string
    {
        $offset = $timezone->getOffset($date) / 60;

        return $offset ? sprintf('%+d', $offset) : '0';
    }

    /**
     * @param \DateTime $date
     * @param string    $month
     *
     * @return int
     */
    public function getDaysInMonth(\DateTime $date, string $month): int
    {
        return cal_days_in_month(CAL_GREGORIAN, MonthEnum::getNumber($month), (int)$date->format('Y'));
    }
}
